# Famicase 2020: Helios

![](assets/helios.jpg)

The Helios game from http://famicase.com/20/index.html

## Ideas

- lighting up dark parts
- darkening too sunny parts
- mixing colors to get a specific color light

## Project description

Idea and cover by Sean Suchara｜Designer / Illustrator｜USA

Helios is a puzzle game about the relationship of light & shadow. Control the environment around you to help direct rays of light through obstacles, to wake up the flowers, illuminate stained glass windows, dance shadows across walls, find the gaps in tree branches and many more pathways to help those lost to find their way out of the dark.

## First prototype: render light and shadows

![](assets/helios-0.1.gif)

## Tasks

- [x] Read a map (a)
- [x] Create a level mechanism (a)
- [ ] Add bitmap sprites? (k)
- [ ] Mirroring lights?
- [ ] Soften the light (k)
- [ ] reduce the length of the light. (k)
- [ ] reduce the angle of the light. (k)
- [ ] Collect ideas for mazes

## Using Godot?

### Light and torch

- [Godot Isometric Tilemap With Light](https://www.youtube.com/watch?v=KzibhXeOa68): realistic light from a torch
- [How to Make 2D Shadows and Light Masks -Now You Know Too](https://www.youtube.com/watch?v=W4O_vNBlH6w): tiles based lighting
- [LIGHT on TILE MAPS](https://www.youtube.com/watch?v=QsCfx-jjS0U): a jump and run with light around the player.
- [Focus on shaddow](https://www.youtube.com/watch?v=ImLBM-NgnJY)
- [1:1 how to make a basic flash light from a torch](https://www.youtube.com/watch?v=hefnwhE-ZZA)
- [theory about implementation of illumination](https://www.youtube.com/watch?v=5gMaushKj6k)
- [Create Awesome 2D Lighting in Godot](https://www.youtube.com/watch?v=7HjdJ9k-jhk)

## Using pygame?

- [Pygame light and shadow casting](https://www.youtube.com/watch?v=L-2onbJfCrc)  
  [Original code in C++](https://www.youtube.com/watch?v=fc3nnG2CG8U)
- [Light Effect PyGame](https://www.youtube.com/watch?v=gPioGZSZeh0): a good example, how to calculate shadow areas (no code)
- [Light from a source and shaddow behind obstacles](https://www.youtube.com/watch?v=L-2onbJfCrc)
- [Tile-based game Part 23: Lighting Effect](https://www.youtube.com/watch?v=IWm5hi5Yrvk): code for a top down light torch

Pygame and tiles:

- [Tile-based game Part 1: Setting up](https://www.youtube.com/watch?v=3UxnelT9aCo)§<https://github.com/kidscancode/pygame_tutorials/>
