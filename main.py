#!/usr/bin/python
from typing import List
from collections import defaultdict
from enum import Enum
import math

from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame
from pygame.locals import *

from settings import *

class Float:
    """ static functions for comparing floats """
    @staticmethod
    def eq(a, b):
        return math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def lte(a, b):
        return a <= b or math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def lt(a, b):
        return a < b and not math.isclose(b, a, abs_tol=1e-06)
    @staticmethod
    def gt(a, b):
        return a > b and not math.isclose(b, a, abs_tol=1e-06)

class Cell:
    def __init__(self, i, j, exist=False):
        self.i = i
        self.j = j
        self.exist = exist
        self.edges = {}

    def draw(self, screen):
        if WIREFRAME:
            return
        pygame.draw.rect(screen, YELLOW, (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE))

Direction = Enum('Direction', 'H V')

class Edge:
    def __init__(self, i, j, side, n=1):
        self.i = i
        self.j = j
        self.side = side
        self.x = i * TILESIZE
        if side == 'E':
            self.x += TILESIZE
        self.y = j * TILESIZE
        if side == 'S':
            self.y += TILESIZE
        if side == 'E' or side == 'W':
            self.direction = Direction.V
        if side == 'N' or side == 'S':
            self.direction = Direction.H
        self.n = n
        self.length = n * TILESIZE

    def __str__(self):
        return f'({self.i}, {self.j}, {self.n})'

    def get_end(self):
        if self.direction == Direction.H:
            return self.x + self.length, self.y
        else:
            return self.x, self.y + self.length

    def grow(self, size=1):
        self.n += size
        self.length += size * TILESIZE

    def draw(self, screen):
        if not WIREFRAME:
            return
        if self.direction == Direction.H:
            pygame.draw.line(screen, GREEN, (self.x, self.y), (self.x + self.length, self.y), 2)
            pygame.draw.circle(screen, RED, (self.x + self.length, self.y), 4)
        else:
            pygame.draw.line(screen, GREEN, (self.x, self.y), (self.x, self.y + self.length), 2)
            pygame.draw.circle(screen, RED, (self.x, self.y + self.length), 4)
        pygame.draw.circle(screen, RED, (self.x, self.y), 4)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __hash__(self):
        # return hash(f'{self.x},{self.y}')
        return self.x * 10000 + self.y

    # def __lt__(self, other):
    #   return (self.y, self.x) < (other.y, other.x)

    def __eq__(self, other):
        return (self.y, self.x) == (other.y, other.x)

    def __str__(self):
        return f'({self.x}, {self.y})'

class LightPolygon:
    def __init__(self, angle, x, y):
        self.angle = angle
        self.x = x
        self.y = y

class Light:
    def __init__(self, i, j, radius):
        self.i = i
        self.j = j
        self.update_x_y()
        self.next_movement = (0, 0)
        self.radius = radius
        self.visibilityPolygonPoints = []
        self.corners = None # cache the values when called from world
        self.edges = None

    def update_x_y(self):
        self.x = self.i * TILESIZE + TILESIZE // 2
        self.y = self.j * TILESIZE + TILESIZE // 2

    def update(self):
        if self.next_movement != (0, 0):
            self.i += self.next_movement[0]
            self.j += self.next_movement[1]
            self.i = min(max(self.i, 0), GRIDWIDTH - 1)
            self.j = min(max(self.j, 0), GRIDHEIGHT - 1)
            self.update_x_y()
            self.next_movement = (0, 0)

            self.calculate_polygons()

    def events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_UP:
                self.next_movement = (0, -1)
            elif event.key == K_DOWN:
                self.next_movement = (0, 1)
            if event.key == K_LEFT:
                self.next_movement = (-1, 0)
            elif event.key == K_RIGHT:
                self.next_movement = (1, 0)

    def draw(self, screen):
        prev_p = self.visibilityPolygonPoints[0]
        for p in self.visibilityPolygonPoints[1:]:
            if WIREFRAME:
                pygame.draw.line(screen, ORANGE, (self.x, self.y), (p[1], p[2]))
            else:
                pygame.draw.polygon(screen, LIGHTORANGE, ((self.x, self.y), (p[1], p[2]), (prev_p[1], prev_p[2])))
            prev_p = p
        p = self.visibilityPolygonPoints[0]
        if WIREFRAME:
            pygame.draw.line(screen, ORANGE, (self.x, self.y), (p[1], p[2]))
        else:
            pygame.draw.polygon(screen, LIGHTORANGE, ((self.x, self.y), (p[1], p[2]), (prev_p[1], prev_p[2])))

        pygame.draw.rect(screen, ORANGE, (self.i * TILESIZE, self.j * TILESIZE, TILESIZE, TILESIZE), 1 if WIREFRAME else 0)



    def cache_corners_edges(self, corners, edges):
        if corners is None:
            if self.corners is not None:
                corners = self.corners
        else:
            self.corners = corners
        if edges is None:
            if self.edges is not None:
                edges = self.edges
        else:
            self.edges = edges

        return corners, edges

    def get_closest_intersection(self, angle, edges):
        """ given the light as origin and an angle to a corner,
            find the closest edge intersecting with the line """
        rdx = self.radius * math.cos(angle)
        rdy = self.radius * math.sin(angle)

        min_t1, min_x, min_y, min_edge = None, None, None, None
        # check the intersection with the edges
        for edge in edges:
            x0, y0 = edge.x, edge.y
            x1, y1 = edge.get_end()
            sdx = x1 - x0
            sdy = y1 - y0

            # normalised distance from line segment start to line segment end of intersect point
            # (vertical and horizontal must be handled separately)
            if rdy == 0 and sdy == 0:
                t1 = 1
            elif Float.eq(rdx, 0.0):
                t1 = (self.y - y0) / 1000
                if self.y < y0:
                    t1 *= -1
            else:
                t2 = (rdx * (y0 - self.y) + rdy * (self.x - x0)) / (sdx * rdy - sdy * rdx)
                # normalised distance from source along ray to ray length of intersect point
                t1 = (x0 + sdx * t2 - self.x) / rdx

                # If no intersect point exists along ray or along line segment
                # then intersect point is not valid
                # if t1 <= 0 or t2 < 0 or t2 > 1.0
                if Float.lte(t1, 0.0) or Float.lt(t2, 0.0) or Float.gt(t2, 1.0):
                    continue
            # reject this intersect point if farther than min
            if not min_t1 is None and t1 >= min_t1:
                continue

            min_t1 = t1
            min_x = self.x + rdx * t1
            min_y = self.y + rdy * t1
            min_edge = edge

        if min_x is None:
            return None, None, None, None

        min_ang = math.atan2(min_y - self.y, min_x - self.x)
        return int(round(min_x)), int(round(min_y)), min_ang, min_edge

    def calculate_polygons(self, corners=None, edges=None):
        """ calculate light triangles by checking where the light rays intersect with the obstacles """
        corners, edges = self.cache_corners_edges(corners, edges)
        if corners is None:
            return

        # TODO: make it a list of LightPolygon?
        self.visibilityPolygonPoints = []

        for point in corners:
            base_angle = math.atan2(point.y - self.y, point.x - self.x)
            x, y, _, _ = self.get_closest_intersection(base_angle, edges)
            if point.x != x or point.y != y:
                continue

            self.visibilityPolygonPoints.append((base_angle, point.x, point.y))

            x, y, _, edge = self.get_closest_intersection(base_angle - 0.0001, edges + World.border_edges)
            if edge is not None:
                x0, y0, x1, y1 = edge.x, edge.y, *edge.get_end()
                if not ((point.x == x0 and point.y == y0) or (point.x == x1 and point.y == y1)):

                    self.visibilityPolygonPoints.append((base_angle - 0.0001, x, y))
            x, y, _, edge = self.get_closest_intersection(base_angle + 0.0001, edges + World.border_edges)

            if edge is not None:
                x0, y0, x1, y1 = edge.x, edge.y, *edge.get_end()
                if not ((point.x == x0 and point.y == y0) or (point.x == x1 and point.y == y1)):

                    self.visibilityPolygonPoints.append((base_angle + 0.0001, x, y))
                    continue
        # sort by the angle
        self.visibilityPolygonPoints.sort(key=lambda i: i[0])

class Level_00:
    def __init__(self):
        self.level_map = Level.read_map(0)

    def get_cells(self):
        cells = [[Cell(i, j, (i, j) in self.level_map['=']) for i in range(GRIDWIDTH)] for j in range(GRIDHEIGHT)]
        return cells

    def get_light(self):
        x, y = self.level_map['*'][0]
        return Light(x, y, 1000)

class Level_01:
    def __init__(self):
        level_map = Level.read_map(1)

class Level:
    levels = [
        lambda: Level_00(),
        lambda: Level_01()
    ]

    def __init__(self, level):
        self.level = level

    @staticmethod
    def create(level):
        return Level.levels[level]()

    @staticmethod
    def read_map(level):
        blocks = defaultdict(list)
        with open('levels/map-00.txt') as f:
            j = 0
            for line in f:
                i = 0
                for c in line.rstrip():
                    if c != ' ':
                        blocks[c].append((i, j))
                    i += 1
                j += 1
        # print(blocks)
        return blocks

class World:
    border_edges = [
        Edge(0, 0, 'N', GRIDWIDTH),
        Edge(GRIDWIDTH - 1, 0, 'E', GRIDHEIGHT),
        Edge(0, GRIDHEIGHT - 1, 'S', GRIDWIDTH),
        Edge(0, 0, 'W', GRIDHEIGHT)
    ]
    border_corners = [
        Point(0, 0),
        Point(WIDTH, 0),
        Point(WIDTH, HEIGHT),
        Point(0, HEIGHT)
    ]
    def __init__(self):
        self.level = Level.create(0)
        self.cells = self.level.get_cells()
        self.edges = []
        # TODO: define the right radius!
        self.light = self.level.get_light()
        self.update_polygons()
        self.light.calculate_polygons(self.get_corners() + World.border_corners, self.edges + World.border_edges)
        self.clicked = False

    def update(self):
        if self.clicked:
            # TODO: we might want to get the pos in events... it's more *exact"
            mouse = pygame.mouse.get_pos()
            self.switch_cell(mouse[0] // TILESIZE, mouse[1] // TILESIZE)
            self.clicked = False
        self.light.update()

    def events(self, event):
        if event.type == MOUSEBUTTONUP:
            self.clicked = True
        else:
            self.light.events(event)

    def draw(self, screen):
        for row in self.cells:
            for cell in row:
                if cell.exist:
                    cell.draw(screen)
        for edge in self.edges:
            edge.draw(screen)
        self.light.draw(screen)

    def get_corners(self):
        """ get a set of unique corners in the edges """
        corners = []
        for edge in self.edges:
            corners.append(Point(edge.x, edge.y))
            corners.append(Point(*edge.get_end()))
        return list(set(corners))

    def switch_cell(self, i, j):
        """ use a cell as obstacle (or not) """
        self.cells[j][i].exist = not self.cells[j][i].exist
        self.update_polygons()
        self.light.calculate_polygons(self.get_corners() + World.border_corners, self.edges + World.border_edges)

    def get_neighbors(self, i, j):
        neighbors = {}
        empty_cell = Cell(0, 0)
        empty_cell.exist = False

        neighbors['N'] = self.cells[j - 1][i] if j > 0 else  empty_cell
        neighbors['E'] = self.cells[j][i + 1] if i < GRIDWIDTH - 1 else  empty_cell
        neighbors['S'] = self.cells[j + 1][i] if j < GRIDHEIGHT - 1 else  empty_cell
        neighbors['W'] = self.cells[j][i - 1] if i > 0 else  empty_cell

        return neighbors

    def extend_or_init_edge(self, cell, side, neighbor):
        if side in neighbor.edges:
            cell.edges[side] = neighbor.edges[side]
            cell.edges[side].grow()
        else:
            edge = Edge(cell.i, cell.j, side)
            self.edges.append(edge)
            cell.edges[side] = edge

    def update_polygons(self):
        """ find the polygons formed by neighboring cells """
        self.edges = []
        for row in self.cells:
            for cell in row:
                cell.edges = {}
        for row in self.cells:
            for cell in row:
                if not cell.exist:
                    continue
                neighbors = self.get_neighbors(cell.i, cell.j)
                if not neighbors['N'].exist:
                    self.extend_or_init_edge(cell, 'N', neighbors['W'])
                if not neighbors['E'].exist:
                    self.extend_or_init_edge(cell, 'E', neighbors['N'])
                if not neighbors['S'].exist:
                    self.extend_or_init_edge(cell, 'S', neighbors['W'])
                if not neighbors['W'].exist:
                    self.extend_or_init_edge(cell, 'W', neighbors['N'])

class Game:
    def __init__(self):
        self.playing = True
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption(TITLE)

        self.clock = pygame.time.Clock()
        self.background = pygame.Surface(self.screen.get_size())
        self.background = self.background.convert()
        self.background.fill(BGCOLOR)

        self.level = Level(0)

        self.world = World()

        self.dt = self.clock.tick(FPS) / 1000

    def update(self):
        self.world.update()

    def draw(self):
        self.screen.blit(self.background, (0, 0))
        if WIREFRAME:
            self.draw_grid()
        self.world.draw(self.screen)
        pygame.display.flip()

    def events(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                self.playing = False
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()
                self.playing = False
            else:
                self.world.events(event)

    def draw_grid(self):
        for x in range(0, WIDTH, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (0, y), (WIDTH, y))


def main():
    game = Game()

    while game.playing:
        game.update()
        game.draw()
        game.events()

if __name__ == '__main__':
    main()
